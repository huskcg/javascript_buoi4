/* 
Exercise 1
input:  1. 3 số nguyên bất kỳ

step:   1. Khởi tạo 3 biến number1, number2, number3
        2. Cho người dùng nhập 3 biến number1, number2, number3
        3. Xét number1 >= number2 && number1 >= number3 => number1 là số lớn nhất. 
                So sánh number2 và number3. 
                Nếu number2 >= number3  thì number2 là số lớn thứ 2, number3 là số nhỏ nhất.
                                        => In ra number3, number2, number1
                Ngược lại               thì number3 là số lớn thứ 2, number2 là số nhỏ nhất.
                                        => In ra number2, number3, number1
        4. Xét number2 >= number1 && number2 >= number3 => number2 là số lớn nhất. 
                So sánh number1 và number3. 
                Nếu number1 >= number3  thì number1 là số lớn thứ 2, number3 là số nhỏ nhất. 
                                        => In ra number3, number1, number2
                Ngược lại               thì number3 là số lớn thứ 2, number1 là số nhỏ nhất.
                                        => In ra number1, number3, number2
        5. Xét number3 >= number1 && number3 >= number2 => number3 là số lớn nhất. 
                So sánh number1 và number2. 
                Nếu number1 >= number2  thì number1 là số lớn thứ 2, number2 là số nhỏ nhất. 
                                        => In ra number2, number1, number3
                Ngược lại               thì number2 là số lớn thứ 2, number1 là số nhỏ nhất.
                                        => In ra number1, number2, number3

output: In ra 3 số nguyên theo thứ tự tăng dần
*/
function calcExcercise1() {
  var number1 = document.getElementById("number1").value * 1;
  var number2 = document.getElementById("number2").value * 1;
  var number3 = document.getElementById("number3").value * 1;
  if (number1 >= number2 && number1 >= number3) {
    if (number2 >= number3) {
      document.getElementById(
        "result-ex1"
      ).innerHTML = `${number3},${number2},${number1}`;
    } else {
      document.getElementById(
        "result-ex1"
      ).innerHTML = `${number2},${number3},${number1}`;
    }
  }
  if (number2 >= number1 && number2 >= number3) {
    if (number1 >= number3) {
      document.getElementById(
        "result-ex1"
      ).innerHTML = `${number3},${number1},${number2}`;
    } else {
      document.getElementById(
        "result-ex1"
      ).innerHTML = `${number1},${number3},${number2}`;
    }
  }
  if (number3 >= number1 && number3 >= number2) {
    if (number1 >= number2) {
      document.getElementById(
        "result-ex1"
      ).innerHTML = `${number2},${number1},${number3}`;
    } else {
      document.getElementById(
        "result-ex1"
      ).innerHTML = `${number1},${number2},${number3}`;
    }
  }
}
/*
Exercise 2
input:  1. Chức vụ của thành viên trong gia đình

step:   1. Khởi tạo biến username
        2. Gán giá trị B,M,A,E lần lượt vào các option của form select của Bố, Mẹ, Anh Trai, Em Gái
        3. Sau khi người dùng chọn chức vụ thì gán giá trị của chức vụ đó vào biến username
        4. Sử dụng switch case 
            => Nếu giá trị bằng "B" thì in ra Xin chào Bố!
            => Nếu giá trị bằng "M" thì in ra Xin chào Mẹ!
            => Nếu giá trị bằng "A" thì in ra Xin chào Anh Trai!
            => Nếu giá trị bằng "E" thì in ra Xin chào Em Gái!
            => Nếu giá trị bằng "" thì in ra Xin chào Người lạ ơi!

output: In ra câu chào hỏi
*/
function calcExcercise2() {
  var username = document.getElementById("select").value;
  switch (username) {
    case "B":
      document.getElementById("result-ex2").innerHTML = `Xin chào Bố!`;
      break;
    case "M":
      document.getElementById("result-ex2").innerHTML = `Xin chào Mẹ!`;
      break;
    case "A":
      document.getElementById("result-ex2").innerHTML = `Xin chào Anh Trai!`;
      break;
    case "E":
      document.getElementById("result-ex2").innerHTML = `Xin chào Em Gái!`;
      break;

    default:
      document.getElementById("result-ex2").innerHTML = `Xin chào Người lạ ơi!`;
      break;
  }
}
/*
Exercise 3
input:  1. 3 số nguyên bất kỳ

step:   1. Khởi tạo 3 biến number1, number2, number3, even, odd
        2. Cho người dùng nhập 3 biến number1, number2, number3
        3. Gán trị even = 0; odd = 0
        4. Xét lần lượt number1%2, number2%2, number3%2.
            Nếu như kết quả dư 0 thì lả chẵn, cộng thêm 1 vào biến even 
                            dư 1 là lẻ, cộng thêm 1 vào biến odd.
        5. In ra kết quả của biến even và biến odd

output: In ra kết quả
*/
function calcExcercise3() {
  var even = 0;
  var odd = 0;
  var number1_ex3 = document.getElementById("number1-ex3").value * 1;
  var number2_ex3 = document.getElementById("number2-ex3").value * 1;
  var number3_ex3 = document.getElementById("number3-ex3").value * 1;
  if (number1_ex3 % 2 == 0) {
    even += 1;
  } else {
    odd += 1;
  }
  if (number2_ex3 % 2 == 0) {
    even += 1;
  } else {
    odd += 1;
  }
  if (number3_ex3 % 2 == 0) {
    even += 1;
  } else {
    odd += 1;
  }
  document.getElementById(
    "result-ex3"
  ).innerHTML = `Có ${even} số chẵn, ${odd} số lẻ`;
}
/*
Exercise 4
input:  1. Độ dài 3 cạnh của tam giác

step:   1. Khởi tạo 3 biến edge1, edge2, edge3.
        2. Cho người dùng nhập dữ liệu rồi gán giá trị vào 3 biến edge1, edge2, edge3.
        3. Kiểm tra tam giác hợp lệ thoả mãn điều kiện:
            edge1 + edge2 > edge3 
                    && 
            edge1 + edge3 > edge2 
                    && 
            edge2 + edge3 > edge1
            ==> Nếu không thoả điều kiện thì không phải là 1 tam giác hợp lệ
        4. Nếu là tam giác hợp lệ, bắt đầu kiểm tra các điểu kiện để xác định hình dạng tam giác
            + edge1 == edge2 && edge2 == edge 3 ==> Tam giác đều
            + edge1 == edge2 || edge2 == edge3 || edge3 == edge1 ==> Tam giác cân
            + edge1^2 = edge2^2 + edge3^2
                    ||
              edge2^2 = edge1^2 + edge3^2
                    ||
              edge3^2 = edge1^2 + edge2^2 ==> Tam giác vuông
            + Ngược lại ==> Tam giác thường
  
output: In ra kết quả
*/
function calcExcercise4() {
  var edge1 = document.getElementById("edge1").value * 1;
  var edge2 = document.getElementById("edge2").value * 1;
  var edge3 = document.getElementById("edge3").value * 1;

  if (edge1 + edge2 > edge3 && edge1 + edge3 > edge2 && edge2 + edge3 > edge1) {
    if (edge1 == edge2 && edge2 == edge3) {
      document.getElementById("result-ex4").innerHTML = `Hình tam giác đều`;
    } else if (edge1 == edge2 || edge2 == edge3 || edge3 == edge1) {
      document.getElementById("result-ex4").innerHTML = `Hình tam giác cân`;
    } else {
      if (
        Math.pow(edge1, 2) == Math.pow(edge2, 2) + Math.pow(edge3, 2) ||
        Math.pow(edge2, 2) == Math.pow(edge1, 2) + Math.pow(edge3, 2) ||
        Math.pow(edge3, 2) == Math.pow(edge1, 2) + Math.pow(edge2, 2)
      ) {
        document.getElementById("result-ex4").innerHTML = `Hình tam giác vuông`;
      } else {
        document.getElementById(
          "result-ex4"
        ).innerHTML = `Một loại tam giác khác`;
      }
    }
  } else {
    alert("Dữ liệu không hợp lệ!");
  }
}
